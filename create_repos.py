#!/usr/bin/python3
import requests
import logging
import sys
import os

HOST = os.getenv('GITLAB', 'https://git.tsinghua.edu.cn')
PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)

auth_header = {
    'Private-Token': PRIV_TOKEN
}

def create_project(name, namespace_id):
    data = {
        'name': name,
        'namespace_id': namespace_id,
        'visibility': 'private',
        'shared_runners_enabled': 'false',
        'lfs_enabled': 'true',
        'emails_disabled': 'true',
    }
    ret = requests.post(HOST+'/api/v4/projects', data, headers=auth_header)
    j = ret.json()
    if ret.status_code == 201:
        print(f"ID {j['id']} name {j['name']} path {j['path_with_namespace']}")
    else:
        print(ret.status_code, j)

def get_group_id(name):
    ret = requests.get(HOST+f'/api/v4/groups?search={name}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and len(j) == 1:
        return j[0]["id"]
    else:
        print(ret.status_code, j)
        return None

def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()
gid = get_group_id('nscscc2021')
if gid is None:
    sys.exit(1)
for i in range(1,201):
    create_project(f'fpga{i:03d}', gid)
