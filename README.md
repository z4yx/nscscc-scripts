## CI准备环节

1. 制作镜像
   1. 安装gitlab-runner和docker：`curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash; sudo apt install gitlab-runner docker.io`
   2. 导入Vivado的Docker镜像 `docker import -c 'USER vivado' -c 'WORKDIR /home/vivado' -c 'ENV HOME /home/vivado' /mnt/vivado2018.3.tar vivado2018:2018.3`；或者从头build Docker镜像，参见https://github.com/z4yx/vivado-docker
   3. 运行一个CI Job来测试
2. 新建一个用户组nscscc2021
2. `create_repos.py`, 批量创建repo，repo 属于组 nscscc2021。脚本使用API（仅供参考）：
   - POST https://gitlab.com/api/v4/projects
   - name=fpga2
     namespace_id=675
     visibility=private
     shared_runners_enabled=false
     lfs_enabled=true
3. `enable_runner_repo.py`, 设置runner。脚本使用API（仅供参考）：
   - GET https://gitlab.com/api/v4/runners
   - POST https://gitlab.com/api/v4/projects/523/runners
   - runner_id=31
4. 创建第一个分支，否则学生无法push。在thinpad_top模板工程目录下执行：
   - `for((i=1;i<=80;i++));do git push git@gitlab.com:nscscc2021/fpga$i.git rev.3:master;done`
5. `set_protected_branches.py`, 设置protected branch
6. `create_accounts.py`, 创建用户
6. `add_an_account_to_repo.py`, 把用户关联到repo
