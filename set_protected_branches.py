#!/usr/bin/python3
import requests
import logging
import sys
import os
import urllib.parse

HOST = os.getenv('GITLAB', 'https://git.tsinghua.edu.cn')
PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)

N_REPO = 200
START_REPO = 1
auth_header = {
    'Private-Token': PRIV_TOKEN
}

def get_project_id(name):
    quoted = urllib.parse.quote(name, safe='')
    ret = requests.get(HOST+f'/api/v4/projects/{quoted}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and ("id" in j):
        return j["id"]
    else:
        print(ret.status_code, j)
        return None

def set_protected_branches(pid):
    branch = 'master'
    ret = requests.delete(HOST+f'/api/v4/projects/{pid}/protected_branches/{branch}', headers=auth_header)
    data = {
        'name': branch,
        'push_access_level': 30,
        'merge_access_level': 30,
    }
    ret = requests.post(HOST+f'/api/v4/projects/{pid}/protected_branches', data, headers=auth_header)
    j = ret.json()
    if ret.status_code == 201:
        return True
    else:
        print(ret.status_code, j)
        return False

def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()

for i in range(START_REPO,START_REPO+N_REPO):
    repo_name = f'nscscc2021/fpga{i:03d}'

    pid = get_project_id(repo_name)
    if pid is None:
        sys.exit(1)

    if not set_protected_branches(pid):
        sys.exit(1)
    else:
        print(f"prj {repo_name} {pid} ok")
