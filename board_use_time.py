#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pymongo
import json
import bisect
from datetime import timezone, timedelta, datetime
from collections import defaultdict
from bson.objectid import ObjectId

def Date(s):
    return datetime.strptime(s, '%Y-%m-%d%z')

client = pymongo.MongoClient(host='localhost', port=27017)
records = client.thinpad_plus.records
results = records.aggregate([
    {'$match': {'$and': [
        {'created_date': {'$gt': Date("2019-11-07+0800")}},
        {'created_date': {'$lt': Date("2019-12-04+0800")}}
        ]}},
    {'$project': {'username': 1, 'created_date': 1, 'close_date': 1, 'duration': {'$subtract': ['$close_date', "$created_date"]}}},
    {'$group': {'_id': '$username',
                'total': {'$sum': '$duration'},
                'count': {'$sum': 1}}},
])
output = defaultdict(dict)
dates = defaultdict(list)
for item in results:
    u = item['_id']
    total_time = item['total']/60000
    count = item['count']
    print(f"{u}\t{total_time:.0f}\t{count}")
    
