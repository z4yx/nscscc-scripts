#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pymongo
import json
import bisect
from datetime import timezone, timedelta
from collections import defaultdict
from bson.objectid import ObjectId

LABS = [ObjectId('5d88f1d416eda253440836e8'),
        ObjectId('5ddd0180e2fc0535f48deb47')]
LAB_NAME = ['Function', 'Challenge']

client = pymongo.MongoClient(host='localhost', port=27017)
coldsubmissions = client.thinpad_plus.coldsubmissions
results = coldsubmissions.aggregate([
    {'$match': {'$and': [{'score': 1}, {'lab_id': {'$in': LABS}}]}},
    {'$group': {'_id': {'user_id': '$user_id', 'lab_id': '$lab_id'},
                'date': {'$min': '$created_date'}}},
    {'$lookup': {'from': 'users', 'localField': '_id.user_id',
                 'foreignField': '_id', 'as': 'user'}},
    {'$project': {'date': 1, 'user.username': 1}}
])
output = defaultdict(dict)
dates = defaultdict(list)
for item in results:
    u = item['user'][0]['username']
    lab = item['_id']['lab_id']
    output[u][lab] = item['date']
    dates[lab].append(item['date'])
    # if u == 'fpga19':
    #     print(u, lab, item['date'])
for i in LABS:
    dates[i].sort()

values = 'username\t'
for i in LAB_NAME:
    values += f"{i}\trank\t"
print(values)
for k,v in output.items():
    def to_localtime_str(d):
        # d = d.astimezone(timezone(timedelta(hours=8)))
        d += timedelta(hours=8)
        return d.strftime("%Y-%m-%d %H:%M:%S %Z%z")
    values = ''
    for i in LABS:
        if i in v:
            values += f"{to_localtime_str(v[i])}\t{bisect.bisect_left(dates[i],v[i])}\t"
        else:
            values += f"Never\tN/A\t"
    print(f"{k}\t{values}")
