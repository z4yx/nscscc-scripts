#!/usr/bin/python3
import requests
import logging
import random
import string
import sys
import os

HOST = os.getenv('GITLAB', 'https://git.tsinghua.edu.cn')
PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)

auth_header = {
    'Private-Token': PRIV_TOKEN
}

def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def create_account(name: str, email: str):
    pwd = randomString()
    data = {
        'name': name,
        'username': name,
        'email': email,
        'password': pwd,
    }
    ret = requests.post(HOST+'/api/v4/users', data, headers=auth_header)
    j = ret.json()
    if ret.status_code == 201:
        print(f"{j['id']}\t{j['name']}\t{pwd}")
    else:
        print(ret.status_code, j)

def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()
for i in range(1,201):
    create_account(f'team{i:03d}', f'team{i:03d}@example.com')
