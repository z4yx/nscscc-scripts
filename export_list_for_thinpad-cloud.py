#!/usr/bin/python3
import requests
import logging
import sys
import os
import urllib.parse
import random
import string

PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)

N_REPO = 79
START_REPO = 1
auth_header = {
    'Private-Token': PRIV_TOKEN
}

def get_project_id(name):
    quoted = urllib.parse.quote(name, safe='')
    ret = requests.get(f'https://git.tsinghua.edu.cn/api/v4/projects/{quoted}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and ("id" in j):
        return j["id"]
    else:
        print(ret.status_code, j)
        return None

def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()

for i in range(START_REPO,START_REPO+N_REPO):
    repo_name = f'nscscc2021/fpga{i}'
    username = f'fpga{i}'
    passwd = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(8)])

    pid = get_project_id(repo_name)
    if pid is None:
        sys.exit(1)

    print(f"{username},{passwd},,{pid}")
