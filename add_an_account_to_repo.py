#!/usr/bin/python3
import requests
import logging
import json
import sys
import os
import re
import urllib.parse

HOST = os.getenv('GITLAB', 'https://git.tsinghua.edu.cn')
PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)
N_REPO = 200
START_REPO = 1

auth_header = {
    'Private-Token': PRIV_TOKEN
}


def get_project_id(name):
    quoted = urllib.parse.quote(name, safe='')
    ret = requests.get(
        HOST+f'/api/v4/projects/{quoted}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and ("id" in j):
        return j["id"]
    else:
        print(ret.status_code, j)
        return None


def get_user_id(name):
    ret = requests.get(
        HOST+f'/api/v4/users?username={name}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and len(j) == 1:
        return j[0]["id"]
    else:
        print(ret.status_code, j)
        return None


def set_project_members(pid, members):
    for uid in members:
        data = {
            'user_id': uid,
            'access_level': 30,
        }
        ret = requests.post(
            HOST+f'/api/v4/projects/{pid}/members', data, headers=auth_header)
        j = ret.json()
        if ret.status_code in [201, 409]:
            continue
        else:
            print(ret.status_code, j)
            return False
    return True

def star_the_project(pid, uid):
    data = {
        'name': f'tmp_star_{pid}',
        'scopes[]': 'api',
    }
    ret = requests.post(
        HOST+f'/api/v4/users/{uid}/impersonation_tokens', data, headers=auth_header)
    tok = ret.json()['token']
    tid = ret.json()['id']
    assert ret.status_code == 201

    u_auth_header = {
        'Private-Token': tok
    }
    ret = requests.post(
        HOST+f'/api/v4/projects/{pid}/star', headers=u_auth_header)
    succ = ret.status_code in (304, 201, 200)

    ret = requests.delete(
        HOST+f'/api/v4/users/{uid}/impersonation_tokens/{tid}', headers=auth_header)
    assert ret.status_code == 204

    return succ

def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()

for i in range(START_REPO,START_REPO+N_REPO):
    repo_name = f'nscscc2021/fpga{i:03d}'
    yhm = f'team{i:03d}'

    pid = get_project_id(repo_name)
    if pid is None:
        print(f"repo {repo_name} not exist")
        break
    uid = get_user_id(yhm)
    if uid is None:
        print(f"user {yhm} not registered on Gitlab")
    print(f"prj {repo_name} {pid} assign {uid}")
    if not set_project_members(pid, [uid]):
        exit()
    if not star_the_project(pid, uid):
        exit()

print("Done")
