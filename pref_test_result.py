#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pymongo
import json
from collections import defaultdict
from bson.objectid import ObjectId

PREF_LAB = ObjectId('5dd4af1ae0669d5ebbe759d0')

client = pymongo.MongoClient(host='localhost', port=27017)
finalresults = client.thinpad_plus.finalresults
results = finalresults.aggregate([
    {'$lookup': {'from': 'coldsubmissions', 'localField': 'submission_id',
                 'foreignField': '_id', 'as': 'res'}},
    {'$lookup': {'from': 'users', 'localField': 'user_id',
                 'foreignField': '_id', 'as': 'user'}},
    {'$project': {'res.score': 1, 'res.result': 1, 'res.lab_id': 1, 'user.username': 1}}
])
output = defaultdict(dict)
for item in results:
    u = item['user'][0]['username']
    # print(u)
    submission = item['res'][0]
    if submission['lab_id'] == PREF_LAB and submission['score'] == 1:
        for brd in submission['result']:
            j = json.loads(brd['result_json'])
            # print(j)
            output[u][j['details']['name']] = j['details']['elapsed']
# print(output)

header = ['1PTB', '2DCT', '3CCT', '4MDCT']
values = '\t'.join(header)
print(f"username\t{values}")
for k,v in output.items():
    values = '\t'.join([f"{v[name]:.2f}" for name in header])
    print(f"{k}\t{values}")
