#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pymongo
import json
import bisect
from datetime import timezone, timedelta
from collections import defaultdict
from bson.objectid import ObjectId

LABS = [ObjectId('5d88f1d416eda253440836e8'),
        ObjectId('5ddd0180e2fc0535f48deb47')]
LAB_NAME = ['Function', 'Challenge']

client = pymongo.MongoClient(host='localhost', port=27017)
coldsubmissions = client.thinpad_plus.coldsubmissions
results = coldsubmissions.aggregate([
    {'$match': {'$and': [{'state': 'Finished'}, {'lab_id': {'$in': LABS}}]}},
    {'$group': {'_id': {'user_id': '$user_id', 'lab_id': '$lab_id'},
                'count': {'$sum': 1}}},
    {'$lookup': {'from': 'users', 'localField': '_id.user_id',
                 'foreignField': '_id', 'as': 'user'}},
    {'$project': {'count': 1, 'user.username': 1}}
])
output = defaultdict(dict)
for item in results:
    u = item['user'][0]['username']
    lab = item['_id']['lab_id']
    output[u][lab] = item['count']

values = 'username\t'
for i in LAB_NAME:
    values += f"{i}\t"
print(values)
for k,v in output.items():
    values = ''
    for i in LABS:
        if i in v:
            values += f"{v[i]}\t"
        else:
            values += f"0\t"
    print(f"{k}\t{values}")
