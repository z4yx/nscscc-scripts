#!/usr/bin/python3
import requests
import logging
import json
import sys
import os
import re
import urllib.parse

PRIV_TOKEN = os.environ["PRIV_TOKEN"]
if not PRIV_TOKEN:
    print("env PRIV_TOKEN not set")
    sys.exit(1)

STU_GROUPS = json.loads(open('stu_groups-final.json').read())
ID_MAPPING = json.loads(open('yhm.json').read())

auth_header = {
    'Private-Token': PRIV_TOKEN
}


def get_project_id(name):
    quoted = urllib.parse.quote(name, safe='')
    ret = requests.get(
        f'https://git.tsinghua.edu.cn/api/v4/projects/{quoted}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and ("id" in j):
        return j["id"]
    else:
        print(ret.status_code, j)
        return None


def get_user_id(name):
    ret = requests.get(
        f'https://git.tsinghua.edu.cn/api/v4/users?username={name}', headers=auth_header)
    j = ret.json()
    if ret.status_code == 200 and len(j) == 1:
        return j[0]["id"]
    else:
        print(ret.status_code, j)
        return None


def set_project_members(pid, members):
    for uid in members:
        data = {
            'user_id': uid,
            'access_level': 30,
        }
        ret = requests.post(
            f'https://git.tsinghua.edu.cn/api/v4/projects/{pid}/members', data, headers=auth_header)
        j = ret.json()
        if ret.status_code in [201, 409]:
            continue
        else:
            print(ret.status_code, j)
            return False
    return True


def enable_debugging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    req_log = logging.getLogger('requests.packages.urllib3')
    req_log.setLevel(logging.DEBUG)

# enable_debugging()


xh2yhm = {}
for item in ID_MAPPING:
    xh2yhm[item['xh']] = item['yhm']

for (grp, members) in STU_GROUPS.items():
    repo_name = f'nscscc2021/{grp}'

    pid = get_project_id(repo_name)
    if pid is None:
        # sys.exit(1)
        print(f"repo {repo_name} not exist")
        continue
    git_members = []
    for xh in members:
        if xh not in xh2yhm:
            print(f"username of {xh} not found")
            continue
        yhm = xh2yhm[xh]
        uid = get_user_id(yhm)
        if uid is None:
            print(f"user {xh} {yhm} not registered on Gitlab")
        else:
            git_members.append(uid)
    # if re.fullmatch(r'nscscc2021/fpga[1-5]', repo_name) is not None:
    print(f"prj {repo_name} {pid} assign {git_members}")
    if not set_project_members(pid, git_members):
        exit()
print("Done")
